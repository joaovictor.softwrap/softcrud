import { auth, firestore, initializeApp, storage, apps, functions } from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/functions';

const firebaseConfig = {
  apiKey: "AIzaSyCRaEvlKux09mz5J_3rNlyVFyyo8ClVwT8",
  authDomain: "class-1-65b9f.firebaseapp.com",
  databaseURL: "https://class-1-65b9f.firebaseio.com",
  projectId: "class-1-65b9f",
  storageBucket: "class-1-65b9f.appspot.com",
  messagingSenderId: "270213644975",
  appId: "1:270213644975:web:a97b6c44377322cf76ca5a"
};

if (!apps.length) {
  initializeApp(firebaseConfig);
}

export const fireAuth = auth();
export const fireStore = firestore();
export const fireStorage = storage();
export const fireFunctions = functions();
