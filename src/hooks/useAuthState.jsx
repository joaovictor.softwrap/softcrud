import { useEffect, useState } from 'react'
import { fireAuth } from '../config/firebase'

const useAuthState = () => {
  const [loggedUser, setLoggedUser] = useState(undefined)
  const [userDate, setUserData] = useState(undefined)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const unsub = fireAuth.onAuthStateChanged((user) => {
      if (user) {
        setLoading(false)
        setLoggedUser(true)
        setUserData(user)
      } else {
        setLoading(false)
        setLoggedUser(false)
      }
    })

    return () => unsub()
  }, [])

  return [loggedUser, userDate, loading]
}

export default useAuthState
