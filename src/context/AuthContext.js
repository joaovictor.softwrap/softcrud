import React, { createContext, useState, useEffect } from 'react';
import { fireAuth, fireStore } from '../config/firebase';

export const AuthContext = createContext({
  loggedUser: false,
  loading: true,
  userData: {},
});

export const AuthProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [loggedUser, setLoggedUser] = useState(undefined);
  const [userData, setUserData] = useState(undefined);

  useEffect(() => {
    const unsub = fireAuth.onAuthStateChanged((user) => {
      if (user) {
        setLoggedUser(user);
        setLoading(false);
      } else {
        setLoggedUser(false);
        setLoading(false);
      }
    });

    return () => unsub();
  }, []);

  useEffect(() => {
    if (loggedUser) {
      const unsub = fireStore
        .collection('users')
        .doc(loggedUser.uid)
        .onSnapshot((user) => {
          if (user && user.data()) {
            setUserData({ id: user.id, ...user.data() });
          }
        });

      return () => unsub();
    }

    return () => {};
  }, [loggedUser]);

  return (
    <AuthContext.Provider value={{ loading, loggedUser, userData }}>
      {children}
    </AuthContext.Provider>
  );
};
