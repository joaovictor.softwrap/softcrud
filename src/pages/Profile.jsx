import React, { useContext } from 'react';
import { View } from 'react-native';
import Header from '../components/Header';
import { AuthContext } from '../context/AuthContext';
import DefaultInput from '../components/DefaultInput';
import { Button } from 'react-native-paper';
import { Formik } from 'formik';
import { fireFunctions } from '../config/firebase';
import Separator from '../components/Separator';

const Profile = ({ onDashboardClick }) => {
  const { userData } = useContext(AuthContext);

  const updateUserData = async (data) => {
    const updateData = fireFunctions.httpsCallable(
      'api-user-updateUserData'
    );

    try {
      await updateData(data);
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <View>
      <Header title="Profile" />
      <Formik
        initialValues={{
          username: userData && userData.username,
          code: userData && userData.code,
        }}
        onSubmit={(values) => updateUserData(values)}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View style={{ padding: 15 }}>
            <DefaultInput
              value={values.username}
              onChangeText={handleChange('username')}
              onBlur={handleBlur('username')}
              label="Username"
            />
            <DefaultInput
              value={values.code}
              onChangeText={handleChange('code')}
              onBlur={handleBlur('code')}
              label="Código"
            />
            <Button
              icon="camera"
              mode="outlined"
              onPress={handleSubmit}
            >
              Editar
            </Button>
            <Separator size={15} />
            <Button
              icon="camera"
              mode="outlined"
              onPress={onDashboardClick}
            >
              Voltar
            </Button>
          </View>
        )}
      </Formik>
    </View>
  );
};

export default Profile;
