import React, { useContext } from 'react'
import {
  View,
  StyleSheet,
  Image,
} from 'react-native'
import { fireAuth, fireStore } from '../config/firebase'
import PickImage from '../components/PickImage'
import Header from '../components/Header';
import { Button } from 'react-native-paper';
import Separator from '../components/Separator';
import { AuthContext } from '../context/AuthContext';

const Logged = ({ onProfileClick }) => {
  const { userData } = useContext(AuthContext);

  return (
    <View>
      <Header
        title="Dashboard"
      />
      <View style={{ padding: 15 }}>
        <Image style={styles.avatar} source={{ uri: userData && userData.cover }} />
        <Separator size={15} />
        <PickImage />
        <Separator size={15} />
        <Button
          icon="exit-run"
          mode="contained"
          onPress={onProfileClick}
        >
          Perfil
        </Button>
        <Separator size={15} />
        <Button
          icon="exit-run"
          mode="contained"
          onPress={() => fireAuth.signOut()}
        >
          Sair
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  avatar: {
    borderColor: '#5a5a5a',
    borderWidth: 4,
    borderRadius: 500,
    width: 200,
    height: 200,
    marginBottom: 5,
  },
})

export default Logged
