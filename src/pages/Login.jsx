import React, { useEffect } from 'react';
import { View, Alert } from 'react-native';
import DefaultInput from '../components/DefaultInput';
import { Button } from 'react-native-paper';
import Header from '../components/Header';
import { fireAuth } from '../config/firebase';
import { useForm } from 'react-hook-form';

const Login = ({ onRegisterClick }) => {
  const {
    register,
    setValue,
    handleSubmit,
    errors,
    trigger,
  } = useForm();

  useEffect(() => {
    register('email', {
      required: 'Campo obrigatorio',
      pattern: {
        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        message: 'Email no formato invalido',
      },
    });
    register('password', { required: 'Campo obrigatorio' });
  }, [register]);

  const loginUser = async (values) => {
    try {
      await fireAuth.signInWithEmailAndPassword(
        values.email,
        values.password
      );
    } catch (e) {
      Alert.alert('Erro !', 'Algo deu errado');
      console.log(e);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title="Login"
        subtitle="Digite suas informações para entrar"
      />
      <View style={{ padding: 15 }}>
        <DefaultInput
          required
          label="E-Mail"
          keyboardType="email-address"
          onChangeText={(emailText) => setValue('email', emailText)}
          onBlur={() => {
            trigger('email');
          }}
          error={errors.email && errors.email.message}
          autoCapitalize="none"
        />
        <DefaultInput
          onChangeText={(password) => setValue('password', password)}
          onBlur={() => {
            trigger('password');
          }}
          required
          error={errors.password && errors.password.message}
          secureTextEntry
          label="Senha"
        />
        <Button
          icon="camera"
          mode="outlined"
          onPress={handleSubmit(loginUser)}
        >
          Entrar
        </Button>
        <Button
          icon="camera"
          mode="outlined"
          onPress={onRegisterClick}
        >
          Registrar
        </Button>
      </View>
    </View>
  );
};

export default Login;
