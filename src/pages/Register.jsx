import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Alert } from 'react-native'
import DefaultInput from '../components/DefaultInput'
import DefaultButton from '../components/DefaultButton'
import { fireAuth, fireStore } from '../config/firebase'
import { Button } from 'react-native-paper';
import Header from '../components/Header';
import { Formik } from 'formik';

const Register = ({ onLoginClick }) => {
  const createAccount = async ({ email, password, confirmPassword }) => {
    if (!email || !password || !confirmPassword) {
      Alert.alert('Atenção', 'Preencha todos os dados')
      return
    }

    if (confirmPassword !== password) {
      Alert.alert('Atenção', 'Senhas diferentes')
      return
    }

    try {
      await fireAuth.createUserWithEmailAndPassword(
        email,
        password
      )
    } catch (e) {
      Alert.alert('Erro !', 'Algo deu errado ' + e.message)
      console.log(e)
    }
  }

  return (
    <View>
      <Header
        title="Registrar"
        subtitle="Digite suas informações para se registrar"
      />
      <Formik
        initialValues={{ email: '' }}
        onSubmit={(values) => createAccount(values)}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View style={{ padding: 15 }}>
            <DefaultInput
              label="E-mail"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
            />
            <DefaultInput
              label="Senha"
              secureTextEntry
              onChangeText={handleChange('password')}
              onBlur={handleBlur('email')}
            />
            <DefaultInput
              label="Confirme a senha"
              secureTextEntry
              onChangeText={handleChange('confirmPassword')}
              onBlur={handleBlur('email')}
            />
            <Button
              icon="camera"
              mode="outlined"
              onPress={handleSubmit}
            >
              Cadastrar
            </Button>
            <TouchableOpacity
              onPress={onLoginClick}
              style={{ alignItems: 'center' }}
            >
              <Text style={{ marginBottom: 10 }}>
                Ja tem conta? Clique aqui.
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </View>
  )
}

export default Register
