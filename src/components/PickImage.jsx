import React, { useState } from 'react'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import DefaultButton from './DefaultButton'
import { fireAuth, fireStorage, fireStore } from '../config/firebase'
import { Button } from 'react-native-paper';

const PickImage = ({ label, onPress }) => {
  const [image, setImage] = useState(undefined);

  const getPermissionAsync = async () => {
    const { status } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    )
    if (status !== 'granted') {
      alert(
        'Desculpe, precisamos de permissão da camera!'
      )
      return false;
    }

    return true;
  }

  const uploadImage = async (uri) => {
    const { uid } = fireAuth.currentUser;

    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = fireStorage.ref().child(`allImages`);
    await ref.put(blob);
    const downloadUrl = await ref.getDownloadURL();

    await fireStore.collection('users').doc(uid).update({
      cover: downloadUrl,
    })
  }

  const _pickImage = async () => {
    const result = await getPermissionAsync();

    if (result) {
      try {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [1, 1],
          quality: 1,
        })
        if (!result.cancelled) {
          setImage(result.uri);
          await uploadImage(result.uri);
        }

        console.log(result)
      } catch (E) {
        console.log(E)
      }
    }
  }

  return (
    <Button icon="camera" mode="outlined" onPress={_pickImage}>
      Trocar a Foto
    </Button>
  );
}

const styles = StyleSheet.create({
  button: {
    marginTop: 10,
    borderRadius: 3,
    marginBottom: 5,
    padding: 15,
    alignItems: 'center',
  },
})

export default PickImage
